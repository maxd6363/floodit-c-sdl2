#ifndef GRID_H
#define GRID_H

#include "block.h"
#include "bool.h"


typedef struct{
	Block **array;
	int size;
}Grid;



Grid grid_init(int size);
void grid_free(Grid *grid);
void grid_print(Grid grid);
void grid_rand(Grid grid, int maxRand);
void grid_spreadColor(Grid grid, Block color);
void internalSpreedColor(Grid grid, Block initColor, Block currentColor, int i, int j);
bool grid_isUniform(Grid grid);
double grid_fillPercentage(Grid grid);
Block grid_getBlock(Grid grid, int x, int y);


#endif