#ifndef GAME
#define GAME


#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "grid.h"
#include "setting.h"


void game_start(Settings settings, SDL_Window *window);

void game_printGame(Grid grid, SDL_Window *window);

void game_end(Grid grid, bool won, SDL_Window *window, TTF_Font *font);

void game_scoreTextProcess(char * text, int currentRound, int maxRound);

#endif