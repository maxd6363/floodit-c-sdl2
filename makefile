#CC : le compilateur à utiliser
CC=gcc

#CFLAGS : les options de compilation  
CFLAGS= -Wall -Wextra -g

# les librairies à utiliser 
LIBS= -lm

#LDFLAGS : les options d'édition de lien
LDFLAGS=-L/usr/local/lib -lSDL2 -lSDL2_ttf

#lieu où se trouve les sources :
SRC=./

#les fichiers objets
OBJ=$(SRC)block.o \
	$(SRC)error.o \
	$(SRC)game.o \
	$(SRC)graphic.o \
	$(SRC)grid.o \
	$(SRC)input.o \
	$(SRC)main.o \
	$(SRC)font.o \
	$(SRC)tools.o \
	$(SRC)setting.o


out : $(OBJ)
	$(CC) $(LDFLAGS) $^ -o $@ $(LIBS)

$(SRC)%.o: $(SRC)%.c $(SRC)%.h
	$(CC) $(CFLAGS)  $(LDFLAGS) -c $< -o $@



clean:
	rm -rf $(SRC)*.o out

