#include "font.h"



TTF_Font * font_init(const char *font, int size){
	TTF_Init();
	return TTF_OpenFont(font, size);
}


void font_generate(TTF_Font *font, char * text, SDL_Renderer *renderer, SDL_Texture ** OUT_fontTexture, SDL_Rect *OUT_fontRect, SDL_Surface ** OUT_fontSurface, int x, int y){
	SDL_Color white = { 255, 255, 255 , 255};
	SDL_Color black = { 0, 0, 0 , 255};
	*OUT_fontSurface = TTF_RenderText_Shaded(font, text, white, black);
	*OUT_fontTexture = SDL_CreateTextureFromSurface(renderer, *OUT_fontSurface);
	int texW = 0;
	int texH = 0;
	SDL_QueryTexture(*OUT_fontTexture, NULL, NULL, &texW, &texH);
	OUT_fontRect->x=x;
	OUT_fontRect->y=y;
	OUT_fontRect->w=texW;
	OUT_fontRect->h=texH;

}


void font_quit(TTF_Font *font, SDL_Texture * fontTexture, SDL_Surface * fontSurface){
	SDL_DestroyTexture(fontTexture);
	SDL_FreeSurface(fontSurface);
	TTF_CloseFont(font);
	TTF_Quit();
}

void font_print(SDL_Texture *fontTexture, SDL_Renderer *renderer, SDL_Rect fontRect){
	SDL_RenderCopy(renderer, fontTexture, NULL, &fontRect);
}

