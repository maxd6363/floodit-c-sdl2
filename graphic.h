#ifndef GRAPHIC_H
#define GRAPHIC_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "block.h"



int graphic_init(void);

void graphic_exit(void);

SDL_Window* graphic_newWindow(const char* title, int width, int height);

void graphic_freeWindow(SDL_Window *window);

SDL_Renderer* graphic_newRenderer(SDL_Window* window);

void graphic_freeRenderer(SDL_Renderer *renderer);


void graphic_clearBackground(SDL_Renderer *renderer);

void graphic_drawBlock(SDL_Renderer *renderer,int widthBlock, int x, int y, Block block);

#endif